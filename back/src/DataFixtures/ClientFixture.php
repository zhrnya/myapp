<?php

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use OAuth2\OAuth2;

class ClientFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $client = new Client();
        $client->setRandomId('2t98pxuykog0o80c8s0kooko848oso84scow4o8g8sswgc0cw0');
        $client->setSecret('3phribnv7pkwwo8s8gww8ws0kss4k0wk0ksoog8okco88gwwc0');
        $client->setAllowedGrantTypes([
            OAuth2::GRANT_TYPE_USER_CREDENTIALS,
            OAuth2::GRANT_TYPE_REFRESH_TOKEN,
        ]);

        $manager->persist($client);
        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
