<?php

namespace App\RestController;

use App\Component\Form\Message;
use App\Entity\Candle;
use App\Form\MessageType;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\RouteResource(resource="candle", pluralize=false)
 */
class CandleController extends AbstractFOSRestController
{
    /**
     * @Rest\Route
     * @Rest\View(serializerGroups={"Default"})
     *
     * @param Request $request
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return Candle|array
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(MessageType::class)
            ->handleRequest($request);

        if (!$form->isSubmitted()) {
            $form->submit([]);
        }

        if (!$form->isValid()) {
            return [
                'form' => $form,
            ];
        }

        /** @var Message $data */
        $data = $form->getData();

        /** @var EntityManager $manager */
        $manager = $this->getDoctrine()->getManager();

        $candle = $data->toCandle();
        $manager->persist($candle);
        $manager->flush();

        return $candle;
    }
}
